# mv

```
Utilisation : mv [OPTION]... [-T] SOURCE DEST
         ou : mv [OPTION]... SOURCE... RÉPERTOIRE
         ou : mv [OPTION]... -t RÉPERTOIRE SOURCE...
Renommer SOURCE en DEST, ou déplacer le ou les SOURCEs vers RÉPERTOIRE.
```
Les arguments obligatoires pour les options longues le sont aussi pour les
options courtes.

```
      --backup[=CONTROL]       archiver chaque fichier de destination existant
  -b                           identique à --backup mais sans argument
  -f, --force                  ne pas demander de confirmation avant d'écraser
  -i, --interactive            demander confirmation avant d'écraser
  -n, --no-clobber             ne pas écraser les fichiers existants
```

Si vous indiquez plusieurs options parmi -i, -f, -n, seule la dernière sera
effective.
```
      --strip-trailing-slashes  enlever les « / » en suffixe de tous les
                                 arguments SOURCE
  -S, --suffix=SUFFIXE         remplacer le suffixe usuel d'archivage
  -t, --target-directory=RÉPERTOIRE  déplacer tous les arguments SOURCE vers
                                     RÉPERTOIRE
  -T, --no-target-directory    traiter DEST comme un fichier normal
  -u, --update                 ne déplacer que si le fichier SOURCE est plus
                                 récent que le fichier cible ou si aucun fichier
                                 cible n'existe
  -v, --verbose                expliquer ce qui est fait
  -Z, --context                définir le contexte de sécurité SELinux du
                                 fichier de destination au type par défaut
                                
      --help     afficher l'aide et quitter
      --version  afficher des informations de version et quitter
```

Le suffixe d'archive est « ~ », sauf s'il est défini autrement avec --suffix ou
SIMPLE_BACKUP_SUFFIX. La méthode du contrôle de version peut être sélectionnée
par l'option --backup ou par la variable d'environnement VERSION_CONTROL.
Les valeurs possibles sont les suivantes :
```
  none, off       ne jamais archiver (même si --backup est utilisée)
  numbered, t     effectuer des archives numérotées
  existing, nil   numéroter si des archives numérotées existent déjà, se
                    comporter comme « simple » dans le cas contraire
  simple, never   effectuer toujours des archives simples
```
Aide en ligne de GNU coreutils : <https://www.gnu.org/software/coreutils/>
Signalez les problèmes de traduction de « mv » à : <traduc@traduc.org>
Documentation complète à : <https://www.gnu.org/software/coreutils/mv>
ou disponible localement via: info '(coreutils) mv invocation